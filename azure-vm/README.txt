https://docs.microsoft.com/de-de/azure/virtual-machines/linux/terraform-create-complete-vm

1) Copy tf file and add credential information.
2) Initialize Terraform to ensure that Terraform has all the prerequisites to build your template in Azure.

terrafrom init

3) Have Terraform review and validate the template. This step compares the requested resources to the state information saved by Terraform and then outputs the planned execution. Resources are not created in Azure.

terraform plan

4) Build the infrastructure in Azure, apply the template in Terraform

terraform apply

5) check VM:
az vm show --resource-group ex532terraformResourceGroup --name ex532terraformVM -d --query [publicIps] --o tsv

6) Login
ssh admin001@<publicIp>

